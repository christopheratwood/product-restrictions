<?php
/*
Plugin Name: Product restrictions
Plugin URI: https://wordpress.org/plugins/perfect-woocommerce-brands/
Description: Allows products restriction messages
Version: 1.1.1
Author: Christopher Atwood
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

add_action( 'init', 'create_restriction_message_taxonomy', 0 );

function create_restriction_message_taxonomy() {

// Labels part for the GUI

 $labels = array(
   'name' => _x( 'Restriction message', 'taxonomy general name' ),
   'singular_name' => _x( 'Restriction message', 'taxonomy singular name' ),
   'search_items' =>  __( 'Search restriction messages' ),
   'popular_items' => __( 'Popular restriction messages' ),
   'all_items' => __( 'All restriction messages' ),
   'parent_item' => null,
   'parent_item_colon' => null,
   'edit_item' => __( 'Edit restriction message' ), 
   'update_item' => __( 'Update restriction message' ),
   'add_new_item' => __( 'Add New restriction message' ),
   'new_item_name' => __( 'New restriction message' ),
   'separate_items_with_commas' => __( 'Separate restriction messages with commas' ),
   'add_or_remove_items' => __( 'Add or remove restriction messages' ),
   'choose_from_most_used' => __( 'Choose from the most used restriction messages' ),
   'menu_name' => __( 'Restriction message' ),
 ); 

// Now register the non-hierarchical taxonomy like tag

 register_taxonomy('restriction_message',array( 'product' ),array(
   'hierarchical' => false,
   'labels' => $labels,
   'show_ui' => true,
   'show_admin_column' => true,
   'update_count_callback' => '_update_post_term_count',
   'query_var' => true,
   'rewrite' => array( 'slug' => 'restriction-message' ),
 ));


}

function productRestrictions() {
  remove_meta_box('restriction_message', array('product'), 'normal');
  add_submenu_page('', __( 'Restriction messages', 'woocommerce' ), __( 'Restriction messages', 'woocommerce' ), 'manage_product_terms', 'restriction_messages', array( $this, 'restrictionMessage' ));
}

add_action('admin_menu','productRestrictions');